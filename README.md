Modèle pivot de data2 
=======================================

# Documentation de l'ontologie mnhn.ttl

# Conventions de nommage

## Prefix

@prefix mnhn:  <https://www.data.mnhn.fr/ontology/>
@prefix mnhnd: <https://www.data.mnhn.fr/data/>

## Graphes nommés


## Construction des URIS

# Références - Etat de l'art

## Ontologies ou modèles intégrés dans le Datahub

### DarwinCore

### Taxref-LD

Pour la description des rangs taxonomiques
