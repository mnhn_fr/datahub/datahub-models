#!/usr/bin/env bash

for i in "asom" "bdlp" "belgicismes" "drf" "franceTerme" "gdt" "inventaire"; 
do 
    echo "running pyshacl -s data-validation_shacl/CSV-dict-validation-shape.trig -e ddf-repository/ddf-mnx.trig -i rdfs -f turtle -o $i-shacl-validation.ttl $i/$i.trig"
    pyshacl -s data-validation_shacl/CSV-dict-validation-shape.trig -e ddf-repository/ddf-mnx.trig -i rdfs -f turtle -o $i-shacl-validation.ttl $i/$i.trig
done